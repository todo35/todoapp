enum Status {
  completed,
  pending,
  rejected,
}

class TodoModel {
  int id;
  int userId;
  String title;
  String dueOn;
  String status;
  TodoModel({
    required this.id,
    required this.userId,
    required this.title,
    required this.dueOn,
    required this.status,
  });
}
