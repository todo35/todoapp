import "package:flutter/material.dart";
import 'package:provider/provider.dart';
import 'package:todo_test_app/screens/add_task.dart';
import 'package:todo_test_app/screens/all_tasks.dart';
import 'package:todo_test_app/screens/search_page.dart';
import 'package:todo_test_app/services/provider.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class MainPage extends StatefulWidget {
  static const routeName = '/main';
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  late TabController _tabController;
  bool _gettingData = true;
  bool _noInternet = false;

  @override
  void initState() {
    getData();
    _tabController = TabController(length: 4, vsync: this);
    super.initState();
  }

  Future<bool> checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult != ConnectivityResult.mobile) {
      return false;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return false;
    } else {
      return true;
    }
  }

  getData() async {
    if (await checkConnection()) {
      //
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('No Internet')),
      );
      setState(() {
        _gettingData = false;
        _noInternet = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        title: const Text('Board'),
        // titleSpacing: 35,
        iconTheme: const IconThemeData(size: 20),
        actions: [
          IconButton(
              onPressed: () {
                Navigator.pushNamed(context, SearchPage.routeName);
              },
              icon: const Icon(Icons.search)),
          IconButton(
              onPressed: () {}, icon: const Icon(Icons.notifications_none)),
          IconButton(onPressed: () {}, icon: const Icon(Icons.menu)),
        ],
      ),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 32),
        child: ElevatedButton(
          onPressed: () {
            Navigator.pushNamed(context, AddTask.routeName);
          },
          child: const Text('Add a tasks'),
        ),
      ),
      body: DefaultTabController(
        length: 4,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 1,
              color: const Color(0xffe1e1e1),
            ),
            TabBar(
              isScrollable: true,
              physics: const BouncingScrollPhysics(),
              // labelPadding: const EdgeInsets.symmetric(horizontal: 5),
              unselectedLabelColor: Colors.grey,
              labelColor: Colors.black,
              indicatorColor: Colors.black,
              indicatorSize: TabBarIndicatorSize.tab,
              controller: _tabController,
              tabs: const [
                Tab(text: 'All'),
                Tab(text: 'Completed'),
                Tab(text: 'Uncompleted'),
                Tab(text: 'Favorite'),
              ],
            ),
            Container(
              height: 1,
              color: const Color(0xffe1e1e1),
            ),
            Expanded(
              child: _noInternet
                  ? Center(child: Text('No Interner'))
                  : TabBarView(
                      physics: const BouncingScrollPhysics(),
                      controller: _tabController,
                      children: [
                        const AllTasks(),
                        Container(),
                        Container(),
                        Container(),
                      ],
                    ),
            ),
          ],
        ),
      ),
    );
  }
}
