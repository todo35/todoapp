import 'dart:math';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_test_app/models/todo.dart';
import 'package:todo_test_app/services/provider.dart';
import 'package:todo_test_app/widgets/custom_drop_down.dart';
import 'package:todo_test_app/widgets/custom_textformfield.dart';
import 'package:todo_test_app/widgets/date_picker.dart';
import 'package:todo_test_app/widgets/time_picker.dart';

class AddTask extends StatefulWidget {
  static const routeName = '/addTask';
  const AddTask({Key? key}) : super(key: key);

  @override
  State<AddTask> createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  late String date;
  late String startTime;
  late String endTime;
  late String remind;
  late String repeat;
  late String title;
  bool validated = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _controller = TextEditingController();
  @override
  Widget build(BuildContext context) {
    var state = Provider.of<TodoAppProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        title: const Text('Add task'),
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back_ios),
        ),
      ),
      body: Form(
        key: _formKey,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Title',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 10),
              CustomTextFormField(
                controller: _controller,
                hintText: 'Design team meeting',
              ),
              const SizedBox(height: 30),
              const Text(
                'DeadLine',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(height: 10),
              DatePickerWidget(
                onSelect: (value) {
                  date = value;
                },
              ),
              const SizedBox(height: 30),
              Row(
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'Start Time',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        const SizedBox(height: 10),
                        TimePickerWidget(
                          onSelect: (value) {
                            startTime = value;
                          },
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(width: 20),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Text(
                          'End Time',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w700,
                          ),
                        ),
                        const SizedBox(height: 10),
                        TimePickerWidget(
                          onSelect: (value) {
                            endTime = value;
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 30),
              CustomDropDownMenu(
                const ['20 minutes early', '30 minutes early', '1 hour early'],
                dropDownValue: '10 minutes early',
                onSelect: (String value) {
                  remind = value;
                },
              ),
              const SizedBox(height: 30),
              CustomDropDownMenu(
                const ['daily', 'weekly', 'monthly'],
                dropDownValue: 'weekly',
                onSelect: (value) {
                  repeat = value;
                },
              ),
              const Expanded(child: SizedBox()),
              ElevatedButton(
                onPressed: () {
                  if (_formKey.currentState!.validate()) {
                    title = _controller.text;
                    state.addTodo(TodoModel(
                      id: Random().nextInt(1000),
                      userId: 50,
                      title: title,
                      dueOn: '',
                      status: 'pending',
                    ));
                    // Fluttertoast.showToast(
                    //   msg: "Added successfully",
                    //   toastLength: Toast.LENGTH_SHORT,
                    //   gravity: ToastGravity.CENTER,
                    //   timeInSecForIosWeb: 1,
                    //   backgroundColor: Colors.black,
                    //   textColor: Colors.white,
                    //   fontSize: 16.0,
                    // );
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(content: Text('Succes')),
                    );
                  }
                },
                child: const Text('Create a task'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
