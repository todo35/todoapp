import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_test_app/services/provider.dart';
import 'package:todo_test_app/widgets/todo_card.dart';

class AllTasks extends StatefulWidget {
  const AllTasks({Key? key}) : super(key: key);

  @override
  State<AllTasks> createState() => _AllTasksState();
}

class _AllTasksState extends State<AllTasks> {
  bool _gettingData = true;

  @override
  void initState() {
    getData();
    super.initState();
  }

  getData() async {
    await Provider.of<TodoAppProvider>(context, listen: false).getAllTodos();
    setState(() {
      _gettingData = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoAppProvider>(
      builder: (context, state, child) {
        return !_gettingData
            ? SingleChildScrollView(
                padding: const EdgeInsets.only(bottom: 50),
                child: Column(
                  children: List.generate(
                    state.todos.length,
                    (index) => Padding(
                      padding: const EdgeInsets.only(bottom: 20),
                      child: TodoCard(state.todos[index]),
                    ),
                  ),
                ),
              )
            : const Center(
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(Colors.green),
                ),
              );
      },
    );
  }
}
