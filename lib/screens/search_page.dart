import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_test_app/services/provider.dart';
import 'package:todo_test_app/widgets/custom_textformfield.dart';
import 'package:todo_test_app/widgets/todo_card.dart';

class SearchPage extends StatefulWidget {
  static const routeName = '/search';
  const SearchPage({Key? key}) : super(key: key);

  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Consumer<TodoAppProvider>(
      builder: (context, state, child) {
        return Scaffold(
          appBar: AppBar(title: Text('Search'), elevation: 0),
          body: Padding(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20),
            child: Column(
              children: [
                CustomTextFormField(
                  hintText: 'Search',
                  autoFocus: true,
                  onChange: (value) {
                    state.search(value!);
                  },
                ),
                SizedBox(height: 20),
                Expanded(
                  child: state.searchTodos.length == 0
                      ? Center(child: Text('No data found'))
                      : ListView.builder(
                          itemCount: state.searchTodos.length,
                          itemBuilder: (context, index) => TodoCard(
                            state.searchTodos[index],
                          ),
                        ),
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
