import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class TimePickerWidget extends StatefulWidget {
  final Function onSelect;
  final bool validated;
  const TimePickerWidget(
      {required this.onSelect, this.validated = false, Key? key})
      : super(key: key);

  @override
  State<TimePickerWidget> createState() => _TimePickerWidgetState();
}

class _TimePickerWidgetState extends State<TimePickerWidget> {
  TimeOfDay selectedTime = TimeOfDay.now();
  bool selected = false;
  // final formatter = DateFormat('yyyy-MM-dd');
  // final formatter = DateFormat.

  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(8),
      onTap: () async {
        selectedTime = await showTimePicker(
              context: context,
              initialTime: TimeOfDay.now(),
              builder: (BuildContext context, Widget? child) {
                return Theme(
                  data: ThemeData(primarySwatch: Colors.green),
                  child: child!,
                );
              },
            ) ??
            TimeOfDay.now();
        widget.onSelect(
          selectedTime.hour.toString() + ' : ' + selectedTime.minute.toString(),
        );
        selected = true;
        setState(() {});
      },
      child: Container(
        height: 54,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          border: Border.all(
            color: widget.validated && !selected
                ? Colors.red
                : const Color(0xffe1e1e1).withOpacity(0.5),
          ),
          borderRadius: BorderRadius.circular(8),
          color: const Color(0xffe1e1e1).withOpacity(0.5),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              selectedTime.hour.toString() +
                  ' : ' +
                  selectedTime.minute.toString(),
            ),
            const Icon(Icons.alarm),
          ],
        ),
      ),
    );
  }
}
