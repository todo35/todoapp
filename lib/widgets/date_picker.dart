import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class DatePickerWidget extends StatefulWidget {
  final Function onSelect;
  bool validated;

  DatePickerWidget({
    required this.onSelect,
    this.validated = false,
    Key? key,
  }) : super(key: key);

  @override
  State<DatePickerWidget> createState() => _DatePickerWidgetState();
}

class _DatePickerWidgetState extends State<DatePickerWidget> {
  DateTime selectedDate = DateTime.now();

  final formatter = DateFormat('yyyy-MM-dd');
  bool selected = false;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      borderRadius: BorderRadius.circular(8),
      onTap: () async {
        selectedDate = await showDatePicker(
              context: context,
              initialDate: DateTime.now(),
              firstDate: DateTime.now(),
              lastDate: DateTime(2023),
              builder: (BuildContext context, Widget? child) {
                return Theme(
                  data: ThemeData(
                    primaryColor: Colors.red,
                    primarySwatch: Colors.amber,
                  ),
                  child: child!,
                );
              },
            ) ??
            DateTime.now();
        widget.onSelect(formatter.format(selectedDate));
        selected = true;
        setState(() {});
      },
      child: Container(
        height: 54,
        padding: const EdgeInsets.symmetric(horizontal: 16),
        decoration: BoxDecoration(
          border: Border.all(
            color: widget.validated && !selected
                ? Colors.red
                : const Color(0xffe1e1e1).withOpacity(0.5),
          ),
          borderRadius: BorderRadius.circular(8),
          color: const Color(0xffe1e1e1).withOpacity(0.5),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(formatter.format(selectedDate)),
            const Icon(Icons.calendar_month_rounded)
          ],
        ),
      ),
    );
  }
}
