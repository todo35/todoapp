import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomDropDownMenu extends StatefulWidget {
  final List<String> valueNames;
  String dropDownValue;
  final Function onSelect;

  bool validated;
  CustomDropDownMenu(
    this.valueNames, {
    Key? key,
    required this.onSelect,
    this.validated = false,
    this.dropDownValue = 'choose',
  }) : super(key: key);

  @override
  CustomDropDownMenuState createState() => CustomDropDownMenuState();
}

class CustomDropDownMenuState extends State<CustomDropDownMenu>
    with TickerProviderStateMixin {
  bool showFirst = true;

  late AnimationController rotationController;

  @override
  void initState() {
    rotationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 200),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(widget.validated);
    return Column(
      children: [
        GestureDetector(
          onTap: () {
            setState(() {
              showFirst = !showFirst;
              if (showFirst) {
                rotationController.reverse();
              } else {
                rotationController.forward();
              }
            });
          },
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: const Color(0xffe1e1e1).withOpacity(0.5),
              border: Border.all(
                color: widget.validated && widget.dropDownValue == 'choose'
                    ? Colors.red
                    : const Color(0xffe1e1e1).withOpacity(0.5),
              ),
            ),
            padding: const EdgeInsets.symmetric(
              vertical: 10,
              horizontal: 15,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Text(
                    widget.dropDownValue,
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                RotationTransition(
                  turns:
                      Tween(begin: 0.0, end: 0.5).animate(rotationController),
                  child: const Icon(Icons.keyboard_arrow_down),
                ),
              ],
            ),
          ),
        ),
        AnimatedCrossFade(
          firstChild: Container(),
          secondChild: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: const Color(0xffe1e1e1).withOpacity(0.5),
              border: Border.all(
                color: const Color(0xffe1e1e1).withOpacity(0.5),
              ),
            ),
            child: Column(
              children: List.generate(
                widget.valueNames.length,
                (index) => Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      setState(() {
                        widget.validated = true;
                        widget.dropDownValue = widget.valueNames[index];
                        showFirst = !showFirst;

                        if (showFirst) {
                          rotationController.reverse();
                        } else {
                          rotationController.forward();
                        }
                      });
                      widget.onSelect(widget.valueNames[index]);
                    },
                    child: Container(
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(
                        vertical: 8,
                        horizontal: 8,
                      ),
                      child: Text(widget.valueNames[index]),
                    ),
                  ),
                ),
              ),
            ),
          ),
          crossFadeState:
              showFirst ? CrossFadeState.showFirst : CrossFadeState.showSecond,
          duration: const Duration(milliseconds: 200),
        ),
      ],
    );
  }
}
