import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_test_app/models/todo.dart';
import 'package:todo_test_app/services/provider.dart';

class TodoCard extends StatefulWidget {
  final TodoModel todo;
  const TodoCard(this.todo, {Key? key}) : super(key: key);

  @override
  State<TodoCard> createState() => _TodoCardState();
}

class _TodoCardState extends State<TodoCard> {
  @override
  Widget build(BuildContext context) {
    var state = Provider.of<TodoAppProvider>(context, listen: false);
    late bool checkboxValue;
    if (widget.todo.status == 'completed') {
      checkboxValue = true;
    } else {
      checkboxValue = false;
    }
    return InkWell(
      onTap: () {
        if (widget.todo.status == 'completed') {
          state.changeStatus(widget.todo.id, 'pending');
        } else {
          state.changeStatus(widget.todo.id, 'completed');
        }
      },
      child: Row(
        children: [
          Checkbox(
            activeColor: Colors.red,
            checkColor: Colors.white,
            materialTapTargetSize: MaterialTapTargetSize.padded,
            // fillColor: MaterialStateColor(),
            side: BorderSide(
              width: 1,
              color: widget.todo.status == 'completed'
                  ? Colors.red
                  : Colors.yellow,
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
            value: checkboxValue,
            onChanged: (value) {
              if (widget.todo.status == 'completed') {
                state.changeStatus(widget.todo.id, 'pending');
              } else {
                state.changeStatus(widget.todo.id, 'completed');
              }
            },
          ),
          Expanded(child: Text(widget.todo.title)),
        ],
      ),
    );
  }
}
