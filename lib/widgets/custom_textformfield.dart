import 'package:flutter/material.dart';

class CustomTextFormField extends StatefulWidget {
  final TextEditingController? textEditingController;
  final Widget? actions;
  final String hintText;
  final TextEditingController? controller;
  final bool autoFocus;
  final String? Function(String?)? validator;
  final Function(String?)? onChange;

  const CustomTextFormField({
    Key? key,
    this.textEditingController,
    this.actions,
    this.controller,
    this.validator,
    this.autoFocus = false,
    this.onChange,
    required this.hintText,
  }) : super(key: key);

  @override
  State<CustomTextFormField> createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: widget.onChange,
      validator: widget.validator ??
          (value) {
            if (value == '') {
              return 'Fill this field';
            } else {
              return null;
            }
          },
      controller: widget.controller,
      cursorColor: Colors.black,
      autofocus: widget.autoFocus,
      decoration: InputDecoration(
        suffixIcon: widget.actions,
        hintText: widget.hintText,
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: BorderSide.none,
        ),
        errorBorder: OutlineInputBorder(
          borderRadius: BorderRadius.circular(8),
          borderSide: const BorderSide(color: Colors.red),
        ),
        filled: true,
        fillColor: const Color(0xffe1e1e1).withOpacity(0.5),
      ),
    );
  }
}
