import 'dart:convert';

import 'package:flutter/cupertino.dart';

import 'package:todo_test_app/models/todo.dart';
import 'package:http/http.dart' as http;

class TodoAppProvider extends ChangeNotifier {
  final String _apiUrl = 'https://gorest.co.in/public/v2/';

  List<TodoModel> _todos = [];
  List<TodoModel> _searchTodos = [];

  List<TodoModel> get todos => _todos;
  List<TodoModel> get searchTodos => _searchTodos;

  void changeStatus(int id, String status) {
    _todos.forEach((element) {
      if (element.id == id) {
        element.status = status;
      }
    });
    notifyListeners();
  }

  void addTodo(TodoModel todo) {
    _todos.add(todo);
    notifyListeners();
  }

  void search(String word) {
    _searchTodos = [];
    _todos.forEach((element) {
      if (element.title.contains(word)) {
        _searchTodos.add(element);
        notifyListeners();
      }
    });
  }

  Future<List<TodoModel>> getAllTodos() async {
    debugPrint('start');
    http.Response response = await http.get(
      Uri.parse('$_apiUrl/todos'),
      headers: {'Accept': 'application/json'},
    );
    _todos = [];
    json.decode(response.body).forEach((todo) {
      _todos.add(
        TodoModel(
          id: todo['id'],
          userId: todo['user_id'],
          title: todo['title'],
          dueOn: todo['due_on'],
          status: todo['status'],
        ),
      );
    });
    return _todos;
  }
}
