import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todo_test_app/screens/add_task.dart';
import 'package:todo_test_app/screens/main_page.dart';
import 'package:todo_test_app/screens/search_page.dart';
import 'package:todo_test_app/services/provider.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => TodoAppProvider(),
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      initialRoute: MainPage.routeName,
      onGenerateRoute: (RouteSettings settings) {
        late Widget page;
        switch (settings.name) {
          case AddTask.routeName:
            page = const AddTask();
            break;
          case SearchPage.routeName:
            page = const SearchPage();
            break;
          default:
            page = const MainPage();
        }
        return PageRouteBuilder(
          transitionsBuilder:
              ((context, animation, secondaryAnimation, child) =>
                  FadeTransition(opacity: animation, child: child)),
          pageBuilder: ((context, animation, secondaryAnimation) => page),
        );
      },
      theme: ThemeData(
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ElevatedButton.styleFrom(
            primary: Colors.green,
            fixedSize: const Size(double.maxFinite, 56),
            onPrimary: Colors.white,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(16),
            ),
          ),
        ),
        colorScheme: const ColorScheme.light(
          primary: Colors.white,
          secondary: Colors.green,
          onPrimary: Colors.black,
        ),
      ),
      home: const MainPage(),
    );
  }
}
